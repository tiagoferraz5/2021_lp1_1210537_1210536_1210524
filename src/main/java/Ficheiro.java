
import java.io.*;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Ficheiro {

    /**
     * Chamar a class ficheiro
     */
    public Ficheiro() {
    }

    /**
     * Se não existir o ficheiro chamado Formadores, ele cria automaticamente,
     * se já existir ele acrescenta ao ficheiro o(s) novo(s) formador(es).
     *
     * @param nome nome do formador
     * @param saidaData data de nascimento do formador
     * @param nif nif do formador
     * @param telefone numero de telefone do formador
     * @param turmas turmas do formador
     * @param codufcd codigo da UFCD do formador
     * @param descufcd descrição da UFCD do formador
     */
    public void EscreverFormadores(String nome, String saidaData, int nif, int telefone, String turmas, int codufcd, String descufcd) {
        try ( FileWriter formadoresFile = new FileWriter("Formadores.txt", true)) {
            formadoresFile.write(nome + ",");
            formadoresFile.write(saidaData + ",");
            formadoresFile.write(nif + ",");
            formadoresFile.write(telefone + ",");
            formadoresFile.write(turmas + ",");
            formadoresFile.write(codufcd + "-");
            formadoresFile.write(descufcd + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Formadores) ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Requalifica ou Aprendizagem, dependendo
     * da idade ele cria automaticamente, se já existir ele acrescenta ao
     * ficheiro selecionado o(s) novo(s) formando(s).
     *
     * @param nome nome do formando
     * @param saidaData data de nascimento do formando
     * @param nif nif do formando
     * @param telefone numero de telefone do formando
     * @param turma turma do formando
     * @param idade idade do formando
     */
    public void EscreverFormandos(String nome, String saidaData, int nif, int telefone, String turma, int idade) {
        String nomeFich;
        if (idade <= 19) {
            nomeFich = "Requalifica.txt";
        } else {
            nomeFich = "Aprendizagem.txt";
        }
        try ( FileWriter formandosFile = new FileWriter(nomeFich, true)) {
            formandosFile.write(nome + ",");
            formandosFile.write(saidaData + ",");
            formandosFile.write(nif + ", ");
            formandosFile.write(telefone + ", ");
            formandosFile.write(turma + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(" + nomeFich + ") ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Ufcd, ele cria automaticamente, se já
     * existir ele acrescenta ao ficheiro a(s) nova(s) Ufcd(s).
     *
     * @param codufcd codigo da UFCD
     * @param descufcd descrição da UFCD
     * @param codcurso codigo do curso dessa UFCD
     */
    public void EscreverUfcd(int codufcd, String descufcd, int codcurso) {
        try ( FileWriter ufcdFile = new FileWriter("Ufcd.txt", true)) {
            ufcdFile.write(codufcd + "-");
            ufcdFile.write(descufcd + ",");
            ufcdFile.write(codcurso + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Ufcd) ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Curso, ele cria automaticamente, se já
     * existir ele acrescenta ao ficheiro o(s) novo(s) curso(s).
     *
     * @param codcurso codigo do curso
     * @param nomecurso nome do curso
     * @param tipocurso tipo de curso (requalificado ou aprendizagem)
     * @param ufcds ufcds desse curso
     */
    public void EscreverCurso(int codcurso, String nomecurso, String tipocurso, int ufcds) {
        try ( FileWriter cursoFile = new FileWriter("Curso.txt", true)) {
            cursoFile.write(codcurso + "-");
            cursoFile.write(nomecurso + ",");
            cursoFile.write(tipocurso + ",");
            cursoFile.write(ufcds + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Curso) ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Turma, ele cria automaticamente, se já
     * existir ele acrescenta ao ficheiro a(s) nova(s) turma(s).
     *
     * @param codturma codigo da turma
     * @param curso curso da turma
     */
    public void EscreverTurma(String codturma, String curso) {
        try ( FileWriter turmaFile = new FileWriter("Turma.txt", true)) {
            turmaFile.write(codturma + ",");
            turmaFile.write(curso + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Turma) ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Nota, ele cria automaticamente, se já
     * existir ele acrescenta ao ficheiro a(s) nova(s) nota(s).
     *
     * @param Ufcd UFCD do formando
     * @param valor valor da nota
     * @param nome nome do formando
     */
    public void EscreverNota(String Ufcd, int valor, String nome) {
        try ( FileWriter notaFile = new FileWriter("Nota.txt", true)) {
            notaFile.write(Ufcd + ",");
            notaFile.write(valor + ",");
            notaFile.write(nome + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Nota) ao escrever...");
        }
    }

    /**
     * Se não existir o ficheiro chamado Documento, ele cria automaticamente, se
     * já existir ele acrescenta ao ficheiro a(s) nova(s) documento(s).
     *
     * @param planodesessao UFCD do formando
     * @param enunciados valor da nota
     * @param notasfinais nome do formando
     */
    public void EscreverDocumento(String planodesessao, String enunciados, int notasfinais) {
        try ( FileWriter notaFile = new FileWriter("Documento.txt", true)) {
            notaFile.write(planodesessao + ",");
            notaFile.write(enunciados + ",");
            notaFile.write(notasfinais + "\n");
        } catch (IOException ex) {
            System.out.println("Apareceu um erro no ficheiro(Nota) ao escrever...");
        }
    }

    /**
     * Lê todas as linhas que tem no ficheiro selecionado pelo utilizador.
     *
     * @param numLeitura numero que o utilizador selecionou
     */
    public void LerFicheiro(int numLeitura) {
        String nomeFich;
        if (numLeitura == 1) {
            nomeFich = "Formadores.txt";
        } else if (numLeitura == 2) {
            nomeFich = "Formandos.txt";
        } else if (numLeitura == 3) {
            nomeFich = "Ufcd.txt";
        } else if (numLeitura == 4) {
            nomeFich = "Curso.txt";
        } else if (numLeitura == 5) {
            nomeFich = "Turma.txt";
        } else if (numLeitura == 6) {
            nomeFich = "Nota.txt";
        } else {
            nomeFich = "Documento.txt";
        }
        try ( BufferedReader leitura = new BufferedReader(new FileReader(nomeFich))) {
            String linha;
            while ((linha = leitura.readLine()) != null) {
                System.out.println(linha);
            }
        } catch (IOException ex) {
            System.out.println("Houve um erro na leitura do ficheiro(" + nomeFich + ")...");
        }
    }

    /**
     * lPA = linha pre ajustada ; lA = linha ajustada ; lR = linha remodelada ;
     * ld = linha desejada
     *
     * Vai buscar o dado que o utilizador selecionou e mete * noutro ficheiro
     * separado e divide os parametros.
     *
     * @param numLinha numero do ficheiro que o utilizador que utilizar
     * @param numLeitura numero de leitura que o utilizador quer guardar
     */
    public void EncontrarLinha(int numLinha, int numLeitura) {
        String nomeFich, EscreverFichNovo, lPAFormador, lAFormador, lRFormador, lDFormador;
        if (numLeitura == 1) {
            nomeFich = "Formadores.txt";
            EscreverFichNovo = "LinhaFormadores.txt";
        } else if (numLeitura == 2) {
            nomeFich = "Formandos.txt";
            EscreverFichNovo = "LinhaFormando.txt";
        } else if (numLeitura == 3) {
            nomeFich = "Ufcd.txt";
            EscreverFichNovo = "LinhaUfcd.txt";
        } else if (numLeitura == 4) {
            nomeFich = "Curso.txt";
            EscreverFichNovo = "LinhaCurso.txt";
        } else if (numLeitura == 5) {
            nomeFich = "Turma.txt";
            EscreverFichNovo = "LinhaTurma.txt";
        } else if (numLeitura == 6) {
            nomeFich = "Nota.txt";
            EscreverFichNovo = "LinhaNota.txt";
        } else {
            nomeFich = "Documento.txt";
            EscreverFichNovo = "LinhaDocumento.txt";
        }
        try ( FileInputStream ficheiro = new FileInputStream(nomeFich);  BufferedReader leitura = new BufferedReader(new InputStreamReader(ficheiro));  FileWriter escritura = new FileWriter(EscreverFichNovo)) {
            for (int i = 0; i < numLinha - 1; ++i) {
                leitura.readLine();
            }
            lPAFormador = leitura.readLine();
            lAFormador = lPAFormador.replace("/", ",");
            lRFormador = lAFormador.replace("-", ",");
            lDFormador = lRFormador.replace(",", "\n");
            escritura.write(lDFormador);
            System.out.println(lPAFormador);
        } catch (IOException ex) {
            System.out.println("Houve um erro no ficheiro(" + EscreverFichNovo + ")...");
        }
    }

    /**
     * Divide as turmas no novo ficheiro chamado TurmasFormado para fazer a
     * pesquisa melhor
     *
     * @return turmas desse formador
     */
    public String CriarTurmaForm() {
        String linhaDesejada, nomeFich, turmas = new String();
        nomeFich = "LinhaFormadores.txt";
        try ( FileInputStream ficheiro = new FileInputStream(nomeFich);  BufferedReader leitura = new BufferedReader(new InputStreamReader(ficheiro));  FileWriter escrever = new FileWriter("TurmasFormador.txt")) {
            for (int i = 0; i < 6; i++) {
                leitura.readLine();
            }
            turmas = leitura.readLine();
            linhaDesejada = turmas.replace(" ", "\n");
            escrever.write(linhaDesejada);

        } catch (IOException ex) {
            System.out.println("Houve um erro no ficheiro(TurmasFormador)...");
        }
        return turmas;
    }

    /**
     * Guarda apenas a turma selecionada pelo utilizador
     *
     * @param numLinha numero da linha desejada
     * @return turma selecionada
     */
    public String GuardarTurmaForm(int numLinha) {
        String nomeFich;
        String turma = null;
        nomeFich = "TurmasFormador.txt";
        try ( FileInputStream ficheiro = new FileInputStream(nomeFich);  BufferedReader leitura = new BufferedReader(new InputStreamReader(ficheiro))) {
            for (int i = 0; i < numLinha - 1; i++) {
                leitura.readLine();
            }
            turma = leitura.readLine();
        } catch (IOException ex) {
            System.out.println("Houve um erro no ficheiro(TurmasFormador)...");
        }
        return turma;
    }

    /**
     * Divide as turmas no novo ficheiro chamado TurmasFormado para fazer a
     * pesquisa melhor Guarda a turma do ficheiro turma
     *
     * @return turmas
     */
    public String GuardarTurma() {
        String nomeFich, turma = new String();
        nomeFich = "LinhaTurma.txt";
        try ( FileInputStream ficheiro = new FileInputStream(nomeFich);  BufferedReader leitura = new BufferedReader(new InputStreamReader(ficheiro));) {
            for (int i = 0; i < 1; i++) {
                leitura.readLine();
            }
            turma = leitura.readLine();
        } catch (IOException ex) {
            System.out.println("Houve um erro ao guardar a turma...");
        }
        return turma;
    }

    /**
     *
     * @param numLeitura
     */
    public void EliminarFicheiro(int numLeitura) {
        String nomeFich;
        if (numLeitura == 1) {
            nomeFich = "Formadores.txt";
        } else if (numLeitura == 2) {
            nomeFich = "Formandos.txt";
        } else if (numLeitura == 3) {
            nomeFich = "Ufcd.txt";
        } else if (numLeitura == 4) {
            nomeFich = "Curso.txt";
        } else if (numLeitura == 5) {
            nomeFich = "Turma.txt";
        } else {
            nomeFich = "Nota.txt";
        }
        File ficheiro = new File(nomeFich);
        if (ficheiro.delete()) {
            System.out.println("O ficheiro: " + ficheiro.getName() + " foi deletado com sucesso!");
        } else {
            System.out.println("Ocurreu um erro na eliminação do ficheiro.");
        }
    }
}
