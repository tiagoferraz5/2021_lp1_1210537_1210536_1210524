/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Pessoas {

    /**
     * Nome da pessoa.
     */
    private String nome;

    /**
     * Nif da pessoa.
     */
    private int nif;

    /**
     * Telefone da pessoa.
     */
    private int telefone;

    /**
     * Data de nascimento da pessoa.
     */
    private String data;

    //-------------------------------------------------------------------------------
    /**
     * Constrói uma intância de formadores recebendo as variaveis nome, dia,
     * mes, ano, nif, telefone.
     *
     * @param nome o nome da pessoa
     * @param data a data de nascimento da pessoa
     * @param nif o nif da pessoa
     * @param telefone o telefone da pessoa
     */
    public Pessoas(String nome, String data, int nif, int telefone) {

        this.nome = nome;
        // this.dia=dia;
        //this.mes=mes;
        //this.ano=ano;
        this.data = data;
        this.nif = nif;
        this.telefone = telefone;
        // this.data=data;

    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve o nome da pessoa.
     *
     * @return nome da pessoa
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve a data de nascimento da pessoa.
     *
     * @return data de nascimento da pessoa
     */
    public String getData() {

        return data;
    }

    /**
     * Devolve o nif da pessoa.
     *
     * @return nif da pessoa
     */
    public int getNif() {
        return nif;
    }

    /**
     * Devolve o telefone da pessoa.
     *
     * @return telefone da pessoa
     */
    public int getTelefone() {
        return telefone;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica o nome da pessoa.
     *
     * @param nome o novo nome da pessoa
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Modifica a date de nascimento da pessoa.
     *
     * @param data a nova data de nascimento da pessoa
     */
    public void setData(String data) {

        this.data = data;
    }

    /**
     * Modifica o nif da pessoa.
     *
     * @param nif o novo nif da pessoa
     */
    public void setNif(int nif) {
        this.nif = nif;
    }

    /**
     * Modifica o telefone da pessoa.
     *
     * @param telefone o novo telefone da pessoa
     */
    public void setTelefone(int telefone) {
        this.telefone = telefone;
    }

}
