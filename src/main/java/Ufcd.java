/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Ufcd {

    /**
     * Codigo da UFCD
     */
    private int codufcd;

    /**
     * Descriçao da UFCD
     */
    private String descufcd;

    /**
     * ID do curso em que a ufcd esta contida
     */
    private int codcurso;

    //-------------------------------------------------------------------------------
    /**
     * @param codufcd codigo da ufcd
     * @param descufcd descriçao da ufcd
     * @param codcurso id do curso
     */
    public Ufcd(int codufcd, String descufcd, int codcurso) {

        this.codufcd = codufcd;
        this.descufcd = descufcd;
        this.codcurso = codcurso;
    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve a data de entrega da UFCD
     *
     * @return Data da UFCD de quando acaba
     */
    public int getCodufcd() {
        return codufcd;
    }

    /**
     * Devolve quando e que havera aulas
     *
     * @return Descrição da UFCD em que o formador esta
     */
    public String getDescufcd() {
        return descufcd;
    }

    /**
     * Devolve o valor do id do curso da ufcd
     *
     * @return ID do curso da ufcd
     */
    public int getCodcurso() {
        return codcurso;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica o codigo da UFCD.
     *
     * @param codufcd o novo codigo da UFCD
     */
    public void setCodufcd(int codufcd) {
        this.codufcd = codufcd;
    }

    /**
     * Modifica a descrição da UFCD
     *
     * @param descufcd a nova descrição da UFCD
     */
    public void setDescufcd(String descufcd) {
        this.descufcd = descufcd;
    }

    /**
     * Modifica o id da ufcd.
     *
     * @param codcurso o novo valor do id do curso
     */
    public void setCodcurso(int codcurso) {
        this.codcurso = codcurso;
    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve a descrição textual da UFCD no formato: codigo - descrição.
     *
     * @return caraterística da UFCD
     */
    public String stringUfcd() {
        return codufcd + " - " + descufcd;
    }

    //-------------------------------------------------------------------------------
    /**
     * Verificação se o ficheiro existe ou não.
     */
    void exists() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
