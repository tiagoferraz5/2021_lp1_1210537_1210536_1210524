/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Curso {

    private int codcurso;
    private String nomecurso;
    private String tipocurso;
    private int ufcds;

    /**
     * @param codcurso codigo do curso de uma turma
     * @param nomecurso nome do curso de uma turma
     * @param tipocurso tipo do curso de uma turma se e maior(Requalifica) de
     * idade ou menor(aprendizagem)
     * @param ufcds id da ufcd para fazer a coneçao
     */
    public Curso(int codcurso, String nomecurso, String tipocurso, int ufcds) {

        this.codcurso = codcurso;
        this.nomecurso = nomecurso;
        this.tipocurso = tipocurso;
        //ciclo for para o array
        this.ufcds = ufcds;
    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve o codigo do curso
     *
     * @return cogigo do curos
     */
    public int getCodcurso() {
        return codcurso;
    }

    /**
     * Devolve o nome do curso
     *
     * @return nome do curso
     */
    public String getNomecurso() {
        return nomecurso;
    }

    /**
     * Devolve o tipo do curso
     *
     * @return Tipo do curso
     */
    public String getTipocurso() {
        return tipocurso;
    }

    /**
     * Devolve as UFCD do curso
     *
     * @return UFCDS
     */
    public int getUfcds() {
        return ufcds;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica o codigo da UFCD.
     *
     * @param codcurso Código do curso
     */
    public void setCodcurso(int codcurso) {
        this.codcurso = codcurso;
    }

    /**
     * Modifica o nome do curso
     *
     * @param nomecurso o nome do curso
     */
    public void setNomecurso(String nomecurso) {
        this.nomecurso = nomecurso;
    }

    /**
     * Modifica o tipo do curso
     *
     * @param tipocurso tipo do curso
     */
    public void setTipocurso(String tipocurso) {
        this.tipocurso = tipocurso;
    }

    /**
     * Modifica as ufcds.
     *
     * @param ufcds ufcds do curso
     */
    public void setUfcds(int ufcds) {
        this.ufcds = ufcds;
    }

    //-------------------------------------------------------------------------------
}
