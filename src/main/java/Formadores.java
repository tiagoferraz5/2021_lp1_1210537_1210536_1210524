/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Formadores extends Pessoas {

    /**
     * Turmas do formador.
     */
    private String turmas;

    /**
     * Codigo da UFCD na qual o formador esta.
     */
    private int codufcd;

    /**
     * Descrição da UFCD na qual o formador esta.
     */
    private String descufcd;

    //-------------------------------------------------------------------------------------------------------------
    /**
     * Constrói uma intância de formadores recebendo as variaveis da extensão
     * "Pessoas" e as variaveis codufcd e descufcd.
     *
     * @param nome o nome do formando
     * @param data a data de nascimento do formador
     * @param nif o nif do formador
     * @param telefone o telefone do formador
     * @param turmas as turmas do formador
     * @param codufcd o codigo da UFCD no qual o formador esta
     * @param descufcd a descrição da UFCD no qual o formador esta
     */
    public Formadores(String nome, String data, int nif, int telefone, String turmas, int codufcd, String descufcd) {

        super(nome, data, nif, telefone);

        this.turmas = turmas;
        this.codufcd = codufcd;
        this.descufcd = descufcd;
    }

    //-------------------------------------------------------------------------------------------------------------
    /**
     * Devolve o codigo da UFCD em que o formador esta.
     *
     * @return Codigo da UFCD em que o formador esta
     */
    public int getCodufcd() {
        return codufcd;
    }

    /**
     * Devolve a descrição da UFCD em que o formador esta.
     *
     * @return Descrição da UFCD em que o formador esta
     */
    public String getDescufcd() {
        return descufcd;
    }

    /**
     * Devolve a descrição da UFCD em que o formador esta.
     *
     * @return Descrição da UFCD em que o formador esta
     */
    public String getTurmas() {
        return turmas;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica as turmas em que o formador esta.
     *
     * @param turmas as novas turmas em que o formador esta
     */
    public void setTurmas(String turmas) {
        this.turmas = turmas;
    }

    /**
     * Modifica o codigo da UFCD em que o formador esta.
     *
     * @param codufcd o novo codigo da UFCD em que o formador esta
     */
    public void setCodufcd(int codufcd) {
        this.codufcd = codufcd;
    }

    /**
     * Modifica a descrição da UFCD em que o formador esta.
     *
     * @param descufcd a nova descrição da UFCD em que o formador esta
     */
    public void setDescufcd(String descufcd) {
        this.descufcd = descufcd;
    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve a descrição textual da UFCD no formato: codigo - descrição.
     *
     * @return caraterística da UFCD
     */
    public String stringUfcd() {
        return codufcd + " - " + descufcd;
    }

    //-------------------------------------------------------------------------------
    /**
     * Verificação se o ficheiro existe ou não.
     */
    void exists() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
