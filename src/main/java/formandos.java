/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class formandos extends Pessoas {

    /**
     * Turma em que o formando esta.
     */
    private String turma;

    //-------------------------------------------------------------------------------
    /**
     * Constrói uma instância de formandos recebendo as variaveis da extensão
     * "Pessoas" e a turma.
     *
     * @param nome o nome do formando
     * @param data a data de nascimento do formando
     * @param nif o nif do formando
     * @param telefone o telefone do formando
     * @param turma a turma do formando
     */
    public formandos(String nome, String data, int nif, int telefone, String turma) {

        super(nome, data, nif, telefone);
        this.turma = turma;

    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve a turma em que o formando esta.
     *
     * @return Turma em que o formando esta
     */
    public String getTurma() {
        return turma;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica a turma em que o formando esta.
     *
     * @param turma a nova em turma que o formando esta
     */
    public void setTurma(String turma) {
        this.turma = turma;
    }

    //-------------------------------------------------------------------------------
    /**
     * Verificação se o ficheiro existe ou não.
     */
}
