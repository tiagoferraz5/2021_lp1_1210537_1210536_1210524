/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Documentos {

    /**
     * Turmas do formador.
     */
    private String planosessao;

    /**
     * Codigo da UFCD na qual o formador esta.
     */
    private String enunciados;

    /**
     * Descrição da UFCD na qual o formador esta.
     */
    private int notasfinais;

    /**
     * Constrói uma intância de Documentos
     *
     * @param planosessao plano de seçao do teste da ufcd
     * @param enunciados o enunciados do teste da ufcd
     * @param notasfinais as noyas finais da ufcd
     */
    public Documentos(String planosessao, String enunciados, int notasfinais) {
        this.planosessao = planosessao;
        this.enunciados = enunciados;
        this.notasfinais = notasfinais;
    }

//-------------------------------------------------------------------------------
    /**
     * Devolve os planos de sessao
     *
     * @return o plano de sessao da ufcd
     */
    public String getPlanosessao() {
        return planosessao;
    }

    /**
     * Devolve os enunciados
     *
     * @return o plano de sessao da ufcd
     */
    public String getEnunciados() {
        return enunciados;
    }

    /**
     * Devolve as notasfinais
     *
     * @return o plano de sessao da ufcd
     */
    public int getNotasfinais() {
        return notasfinais;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica as turmas em que o formador esta.
     *
     * @param planosessao as novas turmas em que o formador esta
     */
    public void setPlanosessao(String planosessao) {
        this.planosessao = planosessao;
    }

    /**
     * Modifica as turmas em que o formador esta.
     *
     * @param enunciados as novas turmas em que o formador esta
     */
    public void setEnunciados(String enunciados) {
        this.enunciados = enunciados;
    }

    /**
     * Modifica as turmas em que o formador esta.
     *
     * @param notasfinais as novas turmas em que o formador esta
     */
    public void setNotasfinais(int notasfinais) {
        this.notasfinais = notasfinais;
    }
}
