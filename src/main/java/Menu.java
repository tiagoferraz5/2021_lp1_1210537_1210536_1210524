
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.*;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Menu {

    /**
     * Classe menu
     * @throws IOException
     */
    public void Menu() throws IOException {

        /*
            Temos de ter menus e sub-menus e term em noção que podem ser pedidas mais funções.
        
           Criar UFCDS que podem ter mais de um professor
           Criar as Turmas que estão dentro da UFCD sendo que a ufcd já tem o professor, não vejo necessidade de o adicioar novamente
           Decidir se dividimos entre aprendizagem para adultos e jovens
           No caso dos alunos temos de guardar: Nome completo • Data de nascimento • NIF • Telefone • Turma em que tá inscrito
           No caso dos profs é necessario guardar: • Nome Completo • Data de nascimento • NIF • Telefone
                                                   • Turmas que lecionam • UFCD que leciona (pode lecionar mais do que uma)
        
           Temos de permitir inserir e alterar as informaçoes relativas aos formandos e formadores
           Guardar as notas obtidas pelos formandos no final das UFCDS que compõem o urso
           As UFCDS TÊM CODIGOS E DESCRIÇÕES
           Cada curso tem 10 UFCDS, os alunos para passarem no curso têm de ter todas as ufcds positivas
           No final de cada UFCD o professor responsavel tem ficheiros para submeter em que deve ser guardado o estado e a data de entrega de cada
                                                    • Plano de sessão • Enunciados testes • Notas Finais
        
           Permitir a consulta/visualização (e não devemos de descartar uma futura função de editar e remover):
                • Notas dos alunos numa UFCD • Notas de um aluno às UFCD já concluídas • Média de um aluno • Média da turma a uma UFCD
                • Documentação entregue ou não de um formador • Listar alunos inscritos numa turma • Listar alunos inscritos numa UFCD
                • Listar UFCD ministradas por um formador
        
         */
        //variaveis
        int i, ins, ins2, f, nif, telefone, codufcd, dia, mes, ano, codcurso, ufcds, valor, idade, lfich, notasfinais;
        LocalDate data;
        String nome, descufcd, turma, curso, nomecurso, tipocurso, codturma, Ufcd, saidaData, turmas = null, planodesessao, enunciados;
        Scanner sc = new Scanner(System.in);
        Scanner sc2 = new Scanner(System.in);
        //________________________________________________________________________________________________

        //criaçao dos array list
        ArrayList<Pessoas> pessoa = new ArrayList<>();
        pessoa.add(new Formadores("Teste1", "10/07/2001", 123456789, 123456789, "a1", 500, "apoo"));
        pessoa.add(new formandos("Teste2", "10/07/2003", 123456789, 123456789, "a1"));

        ArrayList<Ufcd> ufcd = new ArrayList<>();
        ufcd.add(new Ufcd(500, "apoo", 10));

        ArrayList<Curso> cursos = new ArrayList<>();
        cursos.add(new Curso(1, "software agil", "Aprendagem", 500));//erro pq vetores

        ArrayList<Turma> turmas1 = new ArrayList<>();
        turmas1.add(new Turma("a1", "software agil"));

        ArrayList<Nota> nota = new ArrayList<>();
        nota.add(new Nota("apoo", 20, "Teste1"));

        ArrayList<Documentos> documento = new ArrayList<>();
        documento.add(new Documentos("plano da sessao", "Enunciado do teste ", 15));
        //_________________________________________________________________________________________________

        //objeto para introduzir nos formadores
        Formadores pf1 = new Formadores("Teste4", "10/07/2001", 123456789, 123456789, "a3", 500, "apoo");

        //objeto para introduzir nos formandos
        formandos est1 = new formandos("Armindo", "10/07/2001", 123456789, 987654321, "9º D");

        //objeto para contruir nas ufcd
        Ufcd ufcd1 = new Ufcd(155, "padeiro", 100);

        //objeto para introduzir nos cursos
        Curso curso1 = new Curso(155, "colinaria", "maiores", 9);

        //objeto para introduzir nos turma
        Turma turma1 = new Turma("a1", "colinaria");

        //objeto para introduzir nos nota
        Nota nota1 = new Nota("Padeiro", 10.0, "Armindo");

        Documentos documento1 = new Documentos("plano da sessao", "Enunciado do teste ", 15);

        //objeto para criar ficheiros
        Ficheiro fich = new Ficheiro();

        /*  try { //Cria o caminho para o ficheiro em um lugar pre especificado 
            Path path = Paths.get("D:\\Disciplines\\APOO\\1 Semestre\\2 Trabalho\\");
            //java.nio.file.Files;
            Files.createDirectories(path);
            System.out.println("Caminho criado com sucesso!");

            } catch (IOException e) {
             System.err.println("Falha ao criar o caminho!" + e.getMessage());
            }*/
        //inicio do menu 
        System.out.println("\n\nEscolha uma opçao");
        System.out.println("\n                ============================================");
        System.out.println("                  |     1 - Introduzir dentro do ficheiro    |");
        System.out.println("                  |     2 - Fazer uma pequisa com arrays     |");
        System.out.println("                  |     3 - Fazer uma pequisa com ficheiros  |");
        System.out.println("                  |     4 - Eliminar dentro do fichero       |");
        System.out.println("                  |     0 - Sair                             |");
        System.out.println("                  ============================================\n");
        i = sc.nextInt();
        switch (i) {
            //introduzir dentro do ficheiro
            case 1 -> {
                System.out.println("\n                ============================================");
                System.out.println("                  |     1 - Introduzir formadores            |");
                System.out.println("                  |     2 - Introduzir formandos             |");
                System.out.println("                  |     3 - Introduzir ufcd                  |");
                System.out.println("                  |     4 - Introduzir cursos                |");
                System.out.println("                  |     5 - Introduzir turmas                |");
                System.out.println("                  |     6 - Introduzir notas                 |");
                System.out.println("                  |     7 - Introduzir documentos            |");
                System.out.println("                  |     0 - Sair                             |");
                System.out.println("                  ============================================\n");
                ins = sc.nextInt();
                switch (ins) {
                    //inserir dentro dos formadores
                    case 1 -> {
                        do {
                            //nome
                            System.out.println("Introduza o nome do formador: ");
                            nome = sc2.nextLine();
                            pf1.setNome(nome);
                            //data (Fazer limite de dia/mes ex.:max 12meses && >0)
                            System.out.println("Insira data nascimento ");
                            try {
                                System.out.println("Introduza o dia de nascimento do formador: ");
                                dia = sc.nextInt();
                                //pf1.setDia(dia);
                                System.out.println("Introduza o mês de nascimento do formador: ");
                                mes = sc.nextInt();
                                //pf1.setMes(mes); 
                                System.out.println("Introduza o ano de nascimento do formador: ");
                                ano = sc.nextInt();
                                //pf1.setAno(ano);
                                // data formatada e em string 
                                data = LocalDate.of(ano, mes, dia);
                                final DateTimeFormatter formatdata = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                saidaData = formatdata.format(data);
                                //System.out.println(saidaData);
                                pf1.setData(saidaData);

                            } catch (Exception e) {   ////////
                                System.out.println("Erro na data");
                                System.out.println("Introduza uma data novamente");

                                System.out.println("Introduza o dia de nascimento do formador: ");
                                dia = sc.nextInt();
                                System.out.println("Introduza o mês de nascimento do formador: ");
                                mes = sc.nextInt();
                                System.out.println("Introduza o ano de nascimento do formador: ");
                                ano = sc.nextInt();
                                // data formatada e em string
                                data = LocalDate.of(ano, mes, dia);
                                final DateTimeFormatter formatdata = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                saidaData = formatdata.format(data);
                                pf1.setData(saidaData);
                            }///////////////////////////////////////////////////////////////////////
                            LocalDate hoje = LocalDate.now();
                            idade = Period.between(data, hoje).getYears();
                            System.out.println(idade);

                            do {
                                //nif
                                System.out.println("Introduza o numero nif do formador: ");
                                nif = sc.nextInt();
                                pf1.setNif(nif);
                            } while (nif < 99999999 && nif > 1000000000);

                            do {
                                //telefone
                                System.out.println("Introduza o numero de telefone do formador: ");
                                telefone = sc.nextInt();
                                pf1.setTelefone(telefone);
                            } while (nif < 99999999 && nif > 1000000000);

                            //Turma
                            System.out.println("Introduza a turma ou as turma em que o formador estar(separado por espaço)");
                            turmas = sc2.nextLine();
                            //Codigo da UFCD
                            System.out.println("Introduza o codigo da UFCD: ");
                            codufcd = sc.nextInt();
                            pf1.setCodufcd(codufcd);

                            System.out.println("Introduza a descrição da UFCD: ");
                            descufcd = sc2.nextLine();
                            pf1.setDescufcd(descufcd);

                            //nova linha e nova pessoa
                            //fich.CriarEscreverFormadores(nome, dia, mes, ano, nif, telefone, codufcd, descufcd);
                            fich.EscreverFormadores(nome, saidaData, nif, telefone, turmas, codufcd, descufcd);

                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }

                    case 2 -> {
                        do {
                            System.out.println("Introduza o nome do formando: ");
                            nome = sc2.nextLine();
                            est1.setNome(nome);

                            try {
                                System.out.println("Introduza o dia de nascimento do formando: ");
                                dia = sc.nextInt();
                                System.out.println("Introduza o mês de nascimento do formando: ");
                                mes = sc.nextInt();
                                System.out.println("Introduza o ano de nascimento do formando: ");
                                ano = sc.nextInt();
                                // data formatada e em string 
                                data = LocalDate.of(ano, mes, dia);
                                final DateTimeFormatter formatdata = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                saidaData = formatdata.format(data);
                                //System.out.println(saidaData);
                                est1.setData(saidaData);

                            } catch (Exception e) {
                                System.out.println("Erro na data");
                                System.out.println("Introduza uma data novamente");

                                System.out.println("Introduza o dia de nascimento do formando: ");
                                dia = sc.nextInt();
                                System.out.println("Introduza o mês de nascimento do formando: ");
                                mes = sc.nextInt();
                                System.out.println("Introduza o ano de nascimento do formando: ");
                                ano = sc.nextInt();
                                // data formatada e em string
                                data = LocalDate.of(ano, mes, dia);
                                final DateTimeFormatter formatdata = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                saidaData = formatdata.format(data);
                                est1.setData(saidaData);
                            }
                            LocalDate hoje = LocalDate.now();
                            idade = Period.between(data, hoje).getYears();

                            do {
                                //nif
                                System.out.println("Introduza o numero nif do formando: ");
                                nif = sc.nextInt();
                                est1.setNif(nif);
                            } while (nif < 99999999 && nif > 1000000000);

                            do {
                                //telefone
                                System.out.println("Introduza o numero de telefone do formando: ");
                                telefone = sc.nextInt();
                                est1.setTelefone(telefone);
                            } while (nif < 99999999 && nif > 1000000000);

                            //turma
                            System.out.println("Introduza a turma do formando: ");
                            turma = sc2.nextLine();
                            est1.setTurma(turma);

                            fich.EscreverFormandos(nome, saidaData, nif, telefone, turma, idade);
                            //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                        //sair do ficheiro
                    }
                    case 3 -> {
                        do {
                            //codufcd
                            System.out.println("Introduza o codigo da ufcd: ");
                            codufcd = sc.nextInt();
                            ufcd1.setCodufcd(codufcd);

                            //descufcd
                            System.out.println("Introduza a descriçao da ufcd: ");
                            descufcd = sc2.nextLine();
                            ufcd1.setDescufcd(descufcd);

                            //codcurso
                            System.out.println("Introduza o codigo do curso desta ufcd: ");
                            codcurso = sc.nextInt();
                            ufcd1.setCodcurso(codcurso);

                            // fich.CriarEscreverFormandos(nome, dia, mes, ano, nif, telefone, turma);
                            fich.EscreverUfcd(codufcd, descufcd, codcurso);
                            //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }
                    case 4 -> {
                        do {
                            //codcurso
                            System.out.println("Introduza o codigo da ufcd: ");
                            codcurso = sc.nextInt();
                            curso1.setCodcurso(codcurso);

                            System.out.println("Introduza o nome do curso: ");
                            nomecurso = sc2.nextLine();
                            curso1.setNomecurso(nomecurso);

                            //tipocurso
                            System.out.println("Introduza o tipo do curso: ");
                            tipocurso = sc2.nextLine();
                            curso1.setTipocurso(tipocurso);

                            //ufcds
                            System.out.println("Introduza as ufcds deste curso: ");
                            ufcds = sc.nextInt();
                            curso1.setUfcds(ufcds);

                            fich.EscreverCurso(codcurso, nomecurso, tipocurso, ufcds); //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }
                    case 5 -> {
                        do {
                            //codturma
                            System.out.println("Introduza a turma: ");
                            codturma = sc2.nextLine();
                            turma1.setCodturma(codturma);

                            //curso
                            System.out.println("Introduza o curso da turma: ");
                            curso = sc2.nextLine();
                            turma1.setCurso(curso);

                            // fich.CriarEscreverFormandos(nome, dia, mes, ano, nif, telefone, turma);
                            fich.EscreverTurma(codturma, curso);
                            //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }
                    case 6 -> {
                        do {
                            //Ufcd
                            System.out.println("Introduza a ufcd: ");
                            Ufcd = sc2.nextLine();
                            nota1.setUfcd(Ufcd);

                            //valor
                            System.out.println("Introduza o valor da nota na ufcd: ");
                            valor = sc.nextInt();
                            nota1.setValor(valor);

                            //nomealuno
                            System.out.println("Introduza o nome do aluno que teve essa nota: ");
                            nome = sc2.nextLine();
                            turma1.setCurso(nome);

                            fich.EscreverNota(Ufcd, valor, nome);
                            //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }
                    case 7 -> {
                        do {
                            //planodesessao
                            System.out.println("Introduza o plano de sessão: ");
                            planodesessao = sc2.nextLine();
                            documento1.setPlanosessao(planodesessao);

                            //enunciados
                            System.out.println("Introduza o enunciado: ");
                            enunciados = sc2.nextLine();
                            documento1.setEnunciados(enunciados);

                            //notasfinais
                            System.out.println("Introduza a nota final: ");
                            notasfinais = sc.nextInt();
                            documento1.setNotasfinais(notasfinais);

                            fich.EscreverDocumento(planodesessao, enunciados, notasfinais);
                            //Mostrar dados
                            System.out.println("Introduza -1 para parar de correr o codigo");
                            f = sc.nextInt();
                        } while (f != -1);
                    }
                }
            }
            case 2 -> {
                //fazer pesquisa com arrays
                System.out.println("\n                ============================================");
                System.out.println("                  |     1 - Pesquisar formadores             |");
                System.out.println("                  |     2 - Pesquisar formandos              |");
                System.out.println("                  |     3 - Pesquisar ufcd                   |");
                System.out.println("                  |     4 - Pesquisar cursos                 |");
                System.out.println("                  |     5 - Pesquisar turmas                 |");
                System.out.println("                  |     6 - Pesquisar notas                  |");
                System.out.println("                  |     7 - Pesquisar documentos             |");
                System.out.println("                  |     0 - Sair                             |");
                System.out.println("                  ============================================\n");
                ins2 = sc.nextInt();
                switch (ins2) {
                    case 1 -> {
                        Formadores aux = null;
                        for (Pessoas p : pessoa) {
                            if (p instanceof Formadores) {
                                System.out.println("Nome do formador: " + p.getNome() + " Data:" + ((Formadores) p).getData() + " Nif:" + ((Formadores) p).getNif() + " Telefone:" + ((Formadores) p).getTelefone() + " Turmas:" + ((Formadores) p).getTurmas() + " Codigo da ufcd:" + ((Formadores) p).getCodufcd() + "-" + ((Formadores) p).getDescufcd());
                                aux = (Formadores) p;
                            }
                        }
                    }
                    case 2 -> {
                        formandos aux = null;
                        for (Pessoas p : pessoa) {
                            if (p instanceof formandos) {
                                System.out.println("Nome do formando: " + p.getNome() + " Data:" + ((Formadores) p).getData() + " Nif:" + ((Formadores) p).getNif() + " Telefone:" + ((Formadores) p).getTelefone() + " Turmas:" + ((Formadores) p).getTurmas());
                                aux = (formandos) p;
                            }
                        }
                    }
                    case 3 -> {
                        Ufcd aux = null;
                        for (Ufcd p : ufcd) {
                            if (p instanceof Ufcd) {
                                System.out.println("UFCD: " + p.getCodufcd() + "-" + ((Ufcd) p).getDescufcd() + " no curso" + ((Ufcd) p).getCodcurso());
                                aux = (Ufcd) p;
                            }
                        }
                    }
                    case 4 -> {
                        Curso aux = null;
                        for (Curso p : cursos) {
                            if (p instanceof Curso) {
                                System.out.println("Codigo: " + p.getCodcurso() + " curso: " + ((Curso) p).getNomecurso() + " - " + ((Curso) p).getNomecurso() + " UFCD " + ((Curso) p).getUfcds());
                                aux = (Curso) p;
                            }
                        }
                    }
                    case 5 -> {
                        Turma aux = null;
                        for (Turma p : turmas1) {
                            if (p instanceof Turma) {
                                System.out.println(p.getCodturma() + " curso:" + ((Turma) p).getCurso());
                                aux = (Turma) p;
                            }
                        }
                    }
                    case 6 -> {
                        Nota aux = null;
                        for (Nota p : nota) {
                            if (p instanceof Nota) {
                                System.out.println("UFCD " + p.getUfcd() + " valores: " + ((Nota) p).getValor() + " aluno: " + ((Nota) p).getNomealuno());
                                aux = (Nota) p;
                            }
                        }
                    }
                    case 7 -> {
                        Documentos aux = null;
                        for (Documentos p : documento) {
                            if (p instanceof Documentos) {
                                System.out.println("plano de sessao: " + p.getPlanosessao() + " Enunciado " + ((Documentos) p).getEnunciados() + " nota final: " + ((Documentos) p).getNotasfinais());
                                aux = (Documentos) p;
                            }
                        }
                    }
                }
            }
            //pesquisa normal dos ficheiros
            case 3 -> {
                do {
                    System.out.println("\n                ============================================");
                    System.out.println("                  |     1 - Pesquisar formadores             |");
                    System.out.println("                  |     2 - Pesquisar formandos              |");
                    System.out.println("                  |     3 - Pesquisar ufcd                   |");
                    System.out.println("                  |     4 - Pesquisar cursos                 |");
                    System.out.println("                  |     5 - Pesquisar turmas                 |");
                    System.out.println("                  |     6 - Pesquisar notas                  |");
                    System.out.println("                  |     7 - Pesquisar notas                  |");
                    System.out.println("                  ============================================\n");
                    lfich = sc.nextInt();
                } while (lfich != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7);
                System.out.println(lfich);
                fich.LerFicheiro(lfich);
            }
            case 4 -> {
                //introduzir codigo para retirar do ficheiro
                do {
                    System.out.println("\n                ============================================");
                    System.out.println("                  |     1 - Eliminar formadores             |");
                    System.out.println("                  |     2 - Eliminar formandos              |");
                    System.out.println("                  |     3 - Eliminar ufcd                   |");
                    System.out.println("                  |     4 - Eliminar cursos                 |");
                    System.out.println("                  |     5 - Eliminar turmas                 |");
                    System.out.println("                  |     6 - Eliminar notas                  |");
                    System.out.println("                  |     7 - Eliminar notas                  |");
                    System.out.println("                  ============================================\n");
                    lfich = sc.nextInt();
                } while (lfich != 1 && i != 2 && i != 3 && i != 4 && i != 5 && i != 6 && i != 7);
                System.out.println(lfich);
                fich.EliminarFicheiro(lfich);
            }
        }
    }
}
