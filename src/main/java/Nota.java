/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Nota {

    /**
     * Nome da ufcd para para comparar com a descrçao da ufcd
     */
    private String Ufcd;

    /**
     * Nota da ufcd
     */
    private double valor;

    /**
     * Nome do aluno para relacionar as notas com os alunos
     */
    private String nomealuno;

    //-------------------------------------------------------------------------------
    /**
     * @param Ufcd codigo da ufcd
     * @param valor descriçao da ufcd
     * @param nomealuno id do curso
     */
    public Nota(String Ufcd, double valor, String nomealuno) {

        this.Ufcd = Ufcd;
        this.valor = valor;
        this.nomealuno = nomealuno;
    }

    //-------------------------------------------------------------------------------
    /**
     * Devolve a UFCD em que a nota esta contida
     *
     * @return UFCD
     */
    public String getUfcd() {
        return Ufcd;
    }

    /**
     * Devolve valor da nota
     *
     * @return valor
     */
    public double getValor() {
        return valor;
    }

    /**
     * Devolve o nome do aluno para fazer a ligaçao com os formandos
     *
     * @return nomealuno
     */
    public String getNomealuno() {
        return nomealuno;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica a UFCD.
     *
     * @param Ufcd Codigo da Ufcd
     */
    public void setUfcd(String Ufcd) {
        this.Ufcd = Ufcd;
    }

    /**
     * Modifica o valor que teve no teste
     *
     * @param valor
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * Muda o nome do aluno
     *
     * @param nomealuno
     */
    public void setNomealuno(String nomealuno) {
        this.nomealuno = nomealuno;
    }

    //-------------------------------------------------------------------------------
}
