/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author Rodrigo Morais
 * @author Daniel Lima
 * @author Tiago Ferraz
 */
public class Turma {

    /**
     * Codigo da turma
     */
    private String codturma;

    /**
     * Nome do curso que esta contido
     */
    private String curso;

    //-------------------------------------------------------------------------------
    /**
     * @param codturma codigo da turma
     * @param curso curso que a turma esta contida
     */
    public Turma(String codturma, String curso) {
        this.codturma = codturma;
        this.curso = curso;
    }
    //-------------------------------------------------------------------------------

    /**
     * Devolve o codigo de uma turma ex: CTeSP-DAS-ER 2021/2022
     *
     * @return Codigo da turma
     */
    public String getCodturma() {
        return codturma;
    }

    /**
     * Devolve qual curso a turma esta contida
     *
     * @return curso da turma
     */
    public String getCurso() {
        return curso;
    }

    //-------------------------------------------------------------------------------
    /**
     * Modifica o codigo da turma.
     *
     * @param codturma o codigo da turma
     */
    public void setCodturma(String codturma) {
        this.codturma = codturma;
    }

    /**
     * Modifica o curso que a turma esta contida
     *
     * @param curso da turma
     */
    public void setCurso(String curso) {
        this.curso = curso;
    }
}
